const { ipcMain } = require('electron')
const {update_screen_to_image_buffer} = require("../screen/screen_to_image_buffer")
const write_keyboard_leds = require("../leds/write_keyboard_leds")
const write_headset_leds = require("../leds/write_headset_leds")

const fs = require("fs")
let coreProps = fs.readFileSync("C:/ProgramData/SteelSeries/SteelSeries Engine 3/coreProps.json", "utf-8")
let url = "http://" + JSON.parse(coreProps)?.address

module.exports = ()=>{   
    ipcMain.handle('send_screen_info_async', async (event, args) => { 
        let image_buffer = await update_screen_to_image_buffer(args.imageData)
        write_keyboard_leds();
        write_headset_leds(url);
    })
}