
let devices = []
let init = false
const sdk = require('cue-sdk');

//find conncected devices
function find_icue_devices(){   
    devices = []
    init = true 
    const details = sdk.CorsairPerformProtocolHandshake();
    const errCode = sdk.CorsairGetLastError(); 
    const deviceCount = sdk.CorsairGetDeviceCount();

    //loop through devices    
    for (let i = 0; i < deviceCount; ++i) {   
        const info = sdk.CorsairGetDeviceInfo(i);
        devices.push(info)
    }   
    
    return devices
}

function list_icue_devices(){
    if(!init){
        devices = find_icue_devices();
    }
    
    return devices;
}

module.exports.find_icue_devices = find_icue_devices
module.exports.list_icue_devices = list_icue_devices