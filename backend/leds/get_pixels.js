const Jimp = require('jimp');  
const {get_current_screen_to_image_buffer} = require("../screen/screen_to_image_buffer")

function convertBufferToRGB(bmp){
    bmp.image = bmp.bitmap.data
    let rgbArray = []
  
    for (let i = 0; i < (bmp.bitmap.width * bmp.bitmap.height) * 4; i +=4) {
      rgbArray.push({
        "r" : bmp.image[i],
        "g":  bmp.image[i + 1],
        "b": bmp.image[i + 2]
      
      })
    }
  
    return rgbArray
}
  
async function get_pixels(w = 21, h = 9){       
   
    let screenSizeWidth = w 
    let screenSizeHeight = h 
    
    let bufferToImage  = get_current_screen_to_image_buffer()

    //resize image
    bufferToImage = bufferToImage.resize(screenSizeWidth , screenSizeHeight)
    
    //convert to rgb array
    let rgbArray = convertBufferToRGB(bufferToImage)   

    let screenSettings = {
        screenSizeHeight: screenSizeHeight,
        screenSizeWidth: screenSizeWidth,
        rgbArray: rgbArray
    }     

    return screenSettings

}

module.exports.get_pixels = get_pixels