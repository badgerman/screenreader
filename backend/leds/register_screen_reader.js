const fetch = require('node-fetch');

async function register_screen_reader(url){
    
    var raw = JSON.stringify({
      "game": "SCREENREADER",
      "event": "HEALTH",
      "min_value": 0,
      "max_value": 100,
      "icon_id": 1,
      "handlers": [
        {
          "device-type": "headset",
          "zone": "earcups",
          "mode": "context-color",
          "context-frame-key": "rgb"
        }
      ]
    });
    
    var requestOptions = {
      method: 'POST',
      headers: {"Content-Type": "application/json"},
      body: raw,
      redirect: 'follow'
    };
    
    fetch(url + "/bind_game_event", requestOptions)     
      .catch(error => console.log('error', error));
}

module.exports = register_screen_reader