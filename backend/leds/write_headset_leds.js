const {get_pixels} = require("./get_pixels")
const fetch = require('node-fetch');

async function write_headset_leds(url){
    //get pixels
    let pixels = await get_pixels(1, 1)
    pixels = pixels.rgbArray

    //send headset
    var data = JSON.stringify({
        "game": "SCREENREADER",
        "event": "HEALTH",
        "data": {
            "value": 100,
            "frame": {
            "rgb": {
                    "red": parseInt(pixels[0].r),
                    "green": parseInt(pixels[0].g),
                    "blue": parseInt(pixels[0].b)
                }
            }
        }
    });


    let res = fetch(url + "/game_event", {
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: data,
        redirect: 'follow'
    })
    
    .catch(error => console.log('error', error));

    }

module.exports = write_headset_leds