const sdk = require('cue-sdk');
const {list_icue_devices} = require("./get_icue_devices")
const {get_pixels} = require("./get_pixels")

async function write_keyboard_leds(){
    let devices = list_icue_devices()

    let width = 0
    
    for(let i = 0; i < devices.length; i++){

        let device = devices[i]
        //if keyboard
        //if(device.type != 2){
            let device_leds = []
            const ledPositions = sdk.CorsairGetLedPositionsByDeviceIndex(i)
            
            let rowIndexArray = {}

            //loop through leds
            ledPositions.map((led)=>{

                let rowIndex = Math.ceil(led.top /1)*1

                if(!rowIndexArray[rowIndex]){
                    rowIndexArray[rowIndex] = []
                }

                rowIndexArray[rowIndex].push(led)

                if(width < rowIndexArray[rowIndex].length){
                    width = rowIndexArray[rowIndex].length
                }

                device_leds.push({
                    ...led,
                    r: 0, 
                    g: 0, 
                    b: 0,
                    rowIndex: rowIndex
                })

            })

           let height = Object.keys(rowIndexArray).length
           
           let pixels = await get_pixels(width, height)
           pixels = pixels.rgbArray

           //sort rows
           let rows = Object.keys(rowIndexArray).sort(function(a, b) {
                return a.rowIndex - b.rowIndex;
            });

           //loop trought row and write pixel alocation
            rows.map((keyRowIndex, rowIndex)=>{
                let itemsOnRow = device_leds.filter((el)=>{
                    return el.rowIndex == keyRowIndex
                })

                for(let w = 0; w < width; w++){
                    if(itemsOnRow[w]){
                        itemsOnRow[w].r = pixels[w + (width * rowIndex)].r
                        itemsOnRow[w].b = pixels[w + (width * rowIndex)].b
                        itemsOnRow[w].g = pixels[w + (width * rowIndex)].g
                       
                    }
                }              
           })
           
            sdk.CorsairSetLedsColorsBufferByDeviceIndex(i, device_leds)
            sdk.CorsairSetLedsColorsFlushBuffer()


        //}
         
    }
   
}

module.exports = write_keyboard_leds