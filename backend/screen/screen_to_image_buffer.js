let screenIMGBuffer = null;
const Jimp = require('jimp');  

async function update_screen_to_image_buffer(imageBuffer){
    //get screen capture
    let jimpImage = await Jimp.read(Buffer.from(imageBuffer.replace(/^data:image\/png;base64,/, ""), 'base64'))
    // jimpImage = await jimpImage.getBufferAsync(Jimp.MIME_PNG)

    screenIMGBuffer = jimpImage
    return jimpImage
}

function get_current_screen_to_image_buffer(){
    if(!screenIMGBuffer){
        return
    }
    return screenIMGBuffer
}

module.exports.update_screen_to_image_buffer = update_screen_to_image_buffer
module.exports.get_current_screen_to_image_buffer = get_current_screen_to_image_buffer