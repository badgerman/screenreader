const { app, BrowserWindow } = require('electron')
const send_screen_info = require("./backend/ipc/send_screen_info")

function createWindow () {
  const win = new BrowserWindow({
      width: 1200,
      height: 800,
      webPreferences: {
        nodeIntegration: true,
        contextIsolation: false,
      }
  })

  win.loadFile('frontend/index.html')
  win.removeMenu()
  // win.webContents.openDevTools()
}

app.whenReady().then(() => {

  //set up electron function
  send_screen_info();

  createWindow()

  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {      
      createWindow()
    }
  })
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})