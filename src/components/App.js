import React from "react";
const { ipcRenderer } = window.require("electron");
const { desktopCapturer } = window.require('electron')

class App extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            screenSizeHeight: 200,
            screenSizeWidth: 400,
            screenData: null,
            mode:"square",
            modeStyle:{
                pixle: {
                    borderRadius: "100%",
                    margin:"1px"
                }
            },
            screenSources: null,
            selectedSource: null,
            refreshInterval: null
        }

        this.canvas = React.createRef();
    }

    async getScreenPixels(){

        let source = this.state.selectedSource       
        let height = this.state.screenSizeHeight
        let width = this.state.screenSizeWidth

        try {
            const stream = await navigator.mediaDevices.getUserMedia({
                audio: false,
                video: {
                mandatory: {
                        chromeMediaSource: 'desktop',
                        chromeMediaSourceId: source.id,
                        minWidth: 1280,
                        maxWidth: 1280,
                        minHeight: 720,
                        maxHeight: 720
                    }
                }                
            })
            
            const video = document.createElement('video');

            video.srcObject = stream;
            video.onloadedmetadata = () => {
                video.play();
                this.state.refreshInterval = setInterval(() => {
                    
                    const canvas = document.createElement("canvas")
                    canvas.height = height
                    canvas.width = width

                    let context = canvas.getContext('2d')
                    context.drawImage(video, 0, 0, width, height);

                    let data = context.getImageData(0, 0, width, height);
                    let rgbArray = []            
        
                    for (let i = 0; i < (width * height) * 4; i +=4) {
                        rgbArray.push({
                            "r" : data.data[i],
                            "g":  data.data[i + 1],
                            "b": data.data[i + 2],
                            "a": data.data[i + 3]              
                        })
                    }
                   
                    this.setState({
                        rgbArray: rgbArray
                    }, ()=>{
                        ipcRenderer.invoke("send_screen_info_async", {
                            rgbArray: rgbArray,
                            imageData: canvas.toDataURL()
                        })    
                    })                     
                }, 40);
            }
        } 
            
        catch (e) {
            handleError(e)
        }          
     
    }

    async getScreenSources(index = 0){
        let screenSources = await desktopCapturer.getSources({ types: ['window', 'screen'] })

        screenSources = screenSources.filter((source)=>{
            return source.name.includes("Screen")
        })        

        this.setState({
            screenSources: screenSources,
            selectedSource: screenSources[index]
        })  
        
        return screenSources
    }

    async componentDidMount(){
        let screenSources = await this.getScreenSources()
        await this.getScreenPixels()     
    }

    drawScreen(){        
        let pixelCount = 0

        const ctx = this.canvas.current.getContext('2d');      

        if(!this.state.rgbArray){
            return null
        }

        for(let h = 0; h < this.state.screenSizeHeight; h++){
            for(let w = 0; w < this.state.screenSizeWidth; w++){                
                let {r} = this.state.rgbArray[pixelCount]
                let {g} = this.state.rgbArray[pixelCount]
                let {b} = this.state.rgbArray[pixelCount]
                let {a} = this.state.rgbArray[pixelCount]

                ctx.fillStyle = "rgba("+r+","+g+","+b+","+(a/ 255)+")";
                ctx.fillRect( w, h, 1, 1 );

                pixelCount++
            }
        } 

        return null
    }

    renderDropdown(){
        let dropDownRender= []

        if(this.state.rgbArray){
            this.state.screenSources.map((source, i)=>{
                dropDownRender.push(
                    <div 
                        className="screenBox" 
                        key={`${source.id}`}
                        onClick={()=>{

                            clearInterval(this.state.refreshInterval)
                            
                            this.setState({
                                selectedSource: source
                            }, ()=>{
                                this.getScreenPixels()
                            })
                        }}
                    >
                        <img src={source.thumbnail.toDataURL()} />
                        <p>{source.name}</p>
                    </div>                    
                )
            })
        }

        return <div className="screenBoxes">{dropDownRender}</div>;
    }

    render(){
      
        if(this.state.rgbArray){
            this.drawScreen()          
        }

        return (
            <div>
                {this.renderDropdown()}
                <canvas width={this.state.screenSizeWidth + "px"} height={this.state.screenSizeHeight + "px"} ref={this.canvas} style={{width: "100%"}}></canvas>
            </div>
        )
    }
}

export default App