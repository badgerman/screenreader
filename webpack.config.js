
const path = require("path");

module.exports = {
  output: {  
         filename: 'bundle.js',
         path: path.resolve(__dirname, 'frontend/js'),
       },
  watch: true,
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: ["style-loader", "css-loader", "sass-loader"]
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ["babel-loader"]
      }
    ]
  },
  
};